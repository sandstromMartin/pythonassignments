import argparse

addition = lambda x,y : x+y
substraction = lambda x,y : x-y

def checkName(name):
    if "e" not in name:
        raise argparse.ArgumentTypeError("Name must have an e in it")
    return name


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("X",help="An integer value that will be used in our math operation in position X", type=int)
    parser.add_argument("Y", type=int)
    parser.add_argument("Z", type=checkName)
    parser.add_argument("-s","--substract", action="store_true")
    
 
    args = parser.parse_args()
    x = args.X
    y = args.Y
    z = args.Z
    #x,y = int(x), int(y)
    print("x",x)
    print("y", y)
    print("z",z)
    
    print(addition(int(x),int(y)))
    print(substraction(int(x),int(y)))

   
