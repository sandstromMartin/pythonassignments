import turtle
import sys
import argparse
from matplotlib.colors import is_color_like

s = turtle.getscreen() 
t = s.turtles()[0]


s.title("Turtle Program!")
t.pen(shown=False)

def checkShape(input):
    
    if (input=="square" or input=="circle" or input=="triangle"):
        return input
    else: 
        print("Unkown shape. Printing square..")
        input = "square"
        return input
    
def checkColor(input):
        
        if (is_color_like(input) == True):            
            return input
        else: 
            print("Unvalid color. Using the default color red.")
            input="black"
            return input

if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-C", "--COLOR",type=checkColor, help="An optional value that let the user choose color, for example using: -C blue or --COLOR blue. If the argument is emty, the default color will be red.")
    parser.add_argument("SHAPE",type=checkShape, help="An required string value that let the user choose squeare, circle, triangle. Else an error message will be printed")
    parser.add_argument("-v","--VERBOSE",action="store_true")
    args = parser.parse_args()
    
    color = args.COLOR
    shape = args.SHAPE
    verbose = args.VERBOSE
    
    if(verbose == True):
        print(f"Shape:{shape}, Color:{color}")
    
    if (color is None):
        color="red"
        t.pensize(2)
        t.fillcolor(color)
    else:
        #color, shape = "red","triangle"
        t.pensize(2)
        t.fillcolor(color)
    
    if (shape=="square"):  
        t.begin_fill()
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.end_fill()
        
    elif (shape=="circle"):
        t.begin_fill()
        t.circle(50)
        t.end_fill()

    elif (shape=="triangle"):
        t.begin_fill()
        t.forward(100)
        t.right(120)
        t.forward(100)
        t.right(120)
        t.forward(100)
        t.end_fill()
    
    else:
        print("Unknown shape!")

    turtle.done()
    quit()