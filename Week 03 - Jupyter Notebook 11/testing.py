#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import argparse

# python testing.py X Y
# Postional Argument:
# X
# Y
# Optional argument:
# -o filename

addition = lambda x,y : x+y
subtraction = lambda x,y : x-y

def checkname(name):
    if "e" not in name:
        raise argparse.ArgumentTypeError("Name must have an e in it")
    return name

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("X",help="An integer value that will be used in our math operation in position X", type=int) 
    parser.add_argument("Y", type=int)
    parser.add_argument("Z", type=checkname)
    parser.add_argument("-s","--subtract", action="store_true") 

    args = parser.parse_args()
    
    x = args.X
    y = args.Y
    
#     x, y = int(x), int(y)
    
    print("x",x)
    print("y",y)
    print(args.subtract)
    
    print(addition(int(x),int(y)))
    if args.subtract: print(subtraction(int(x),int(y)))


