import argparse

x=10
y=5
z=1

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("X")
    parser.add_argument("Y")
    parser.add_argument("Z")

    args = parser.parse_args()

    x = args.X
    y = args.Y
    z = args.Z

    x, y, z = int(x), int(y), int(z)

    print("x",x)
    print("y",y)
    print("z",z)