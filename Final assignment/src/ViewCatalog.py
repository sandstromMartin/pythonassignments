'''ViewCatalog.py

A Python script written for UKEA Shopping.

A script that displays the current index of items'''

import os
import json

def print_inventory_item(name,item):
    '''Pretty prints the given name and item to the console'''
    max_key_len = max([len(x) for x in item.keys()]) + 1
    max_val_len = max([len(str(x)) for x in item.values()]) + 1
    
    # upper() function convert the name value into uppercase letters
    print(f"| {name.upper()} "+"-"*(max_key_len+max_val_len-len(name))+" |")
    
    for (k,v) in item.items():
        
        # the pretty print
        if isinstance(v,str) and len(v) > 49:
            # if so, we use the rfind function to check the last blank space in between first and the 50:th char
            lastpos = v.rfind(" ",0,49)
            # we can then seperate the string in two varaibles. The one between 0-50 and the rest into another one. 
            firstString = v[:lastpos]
            secondString = v[lastpos+1:]
            # An extra empty string, since the f" was crying about it... to match the layout. 
            empty = " "
 
            # the first print with the key values and the first string up to 50 cars. And the second print only with the rest of the string.
            print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(firstString):>{max_val_len}} |")
            print(f"| {empty:<{max_key_len}} {str(secondString):>{max_val_len}} |")

        # check all items in the list before converting the value
        elif isinstance(v, list):
            temp=""
            for m in v:
                temp += str("%.2f" % m) +"m x"               
            v=temp[:-3]
            
            print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}} |")
        elif isinstance(v, int):
            if v < 20 and v > 9:
                v = f"Only 20 left"
            elif v < 10 and v > 4:
                v = f"Only 10 left"
            elif v < 5 and v > 0:
                v = f"Only 5 left"
            elif v == 0:
                v = f"OUT OF STOCK"
            print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}} |")
        elif isinstance(v,float):
            v = str("%.2f" % v)
            v= f"{v} SEK"
            print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}} |")
        else:
            print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}} |")


def main():
    # Starting up print statement
    print("Viewing inventory...\n")
    try: 
        with open("data/inventory.json","r") as inputFile:
            print("File open")
            inventory = json.load(inputFile)
            print("file load")
            # Print all items in inventory 
            for k,v in inventory.items():
                print_inventory_item(k, v) 
            
            # Print and empty string to make things spaced out
            print("")
    except:
        return "Could not find file! "

if __name__ == '__main__':
    main()

