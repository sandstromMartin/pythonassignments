import os 
import json
import re
import datetime

def printShoppingCart(inventory):

    items1Counter = 0
    items2Counter = 0
    items3Counter = 0

    item1String=None
    item2String=None
    item3String=None


    if inventory is None:
        print("Cart is empyt, keep shopping.")
    else:
        for itemList in inventory:
            for k,v in itemList.items():
                if k == "Froggy Chair":
                    items1Counter +=1
                    item1String = (f"| {k} {v['cost']} SEK x {items1Counter} Total:{v['cost']*items1Counter} SEK |")
                elif k == "Mega Desk":
                    items2Counter +=1
                    item2String = (f"| {k} {v['cost']} SEK x {items2Counter} Total:{v['cost']*items2Counter} SEK |")
                   
                elif k == "Lava Solar Lamp":
                    items3Counter +=1
                    item3String = (f"| {k} {v['cost']} SEK x {items3Counter} Total:{v['cost']*items3Counter} SEK |")
                    #print(f"| {k.replace('_',' ').capitalize():<{max_key_len}}:{str(firstString):>{max_val_len}} |")              
        
        if item1String is not None:
            print(item1String)
        if item2String is not None:    
            print(item2String)
        if item3String is not None:
            print(item3String)

    # Print and empty string to make things spaced out
    print("")

def handleLenght(cC):

    if len(cC)==16:
        return True
    else:
        return False

def handleCredtCard():

    # while loop to let the customer try again if the input value is not valid
    while True:
        creditCard = input("Please enter credit card number...")
        
        creditCard = creditCard.replace(" ","")
        if creditCard == "q":
            print("ok bye")
            break
        pattern = re.compile("[0-9]+")
        
        if pattern.fullmatch(creditCard) is not None:
            if(handleLenght(creditCard)):
                print("Valid number")
                nameHandler()
                break
            else:
                print("Plsease input a valid lenght of credit card number.")
        else:
            print("Cant include numbers (0-9). Only numbers")
       
def nameHandler():
    while True:
        surname = input("Please enter name of the credit card owner")
        surname = surname.replace("-", "")
        if surname == "q":
            print("ok bye")
            break
        if surname.isalpha() or "-" in surname:
            if surname == surname.upper():
                print("Valid card owner")
                cvvHandler()
                break
            elif surname != surname.upper():
                print("Name needs to be UPPERCASE. try again")
        else:
            print("Name can only include alpha and \"-\" letter. Try again")

def cvvHandler():
    while True:
        cvv = input("Please enter cvv.")
        if cvv == "q":
            print("ok bye")
            break
        
        pattern = re.compile("[0-9]+")

        if pattern.fullmatch(cvv) is not None:
            if len(cvv) == 3:
                print("Valid cvv")
                transactionHandler()
                break
            else:
                print("cvv needs to be exactly 3 numbers")
        
        else: 
            print("Only numbers ofcourse....!")
 
# when the credit card is valid, the transation function print the time date to transaction file and 
# recreate the shoppingcart file as an empty file.
def transactionHandler():
    print("Payment SUCCESS")
    try:
        with open("data/transactions.log","a+") as tFileOut: 
            tFileOut.write(str( datetime.datetime.now())+"\n")
            tFileOut.close()
            open("data/shoppingCart.json","w").close()

    except:
        print("Transaction fail. Abort mission.")

def main():
  
    print("Your shopping cart...\n")
    try: 
        with open("data/shoppingCart.json","r") as inputFile:
            # store the json obejcts into inventory variable
            inventory = json.load(inputFile)

            # Print the shopping cart function and calling on the function for payment
            printShoppingCart(inventory)
            handleCredtCard()

            
    except:
        return "Could not find file! "

if __name__ == '__main__':
    main()

