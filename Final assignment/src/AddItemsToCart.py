
import os
import json
import re 


def handlePurchase(name, item, amount):

    cartingList= []
    counter = 0

    # regulate the instock of the product
    item['instock'] = item['instock'] - amount
    print(f"{amount} items has been added to your cart. {item['instock']} left")

    # the item that will be added to our cart
    newItem = {"product_id":item['product_id'], "cost": item['cost'], "description": item['description']}
    product = {name: newItem}
  
    try:
        with open("data/shoppingCart.json", "r") as inputCart:               
            jsDict = json.load(inputCart)
            # read existing cart
            inputCart.close()
            cartingList=jsDict
            
            while amount > counter:
                cartingList.append(product)
                counter+=1
                
            # write the new list of json objects
            with open("data/shoppingCart.json", "w") as outputCart:
                
                outputCart.seek(0)
                json.dump(cartingList, outputCart, indent=3)
               
                outputCart.close()

    except:
        print("Created cart")
        with open("data/shoppingCart.json", "w") as outputCart:
        
            while amount > counter:
                cartingList.append(product)
                counter+=1
            json.dump(cartingList, outputCart, indent=3)
                  
def handleInventory(inputId, inventory):
    invalid = None
    inStock = None
    purchase = 0
    purchaseCounter = 0
    cancel = None
   

    for k,v in inventory.items():      
        
        if v["product_id"] == inputId:
            # INFO: Break point if the id can be found
            invalid = False
            

            if v["instock"] > 0:
                print(f"{k}, cost {v['cost']}SEK and {v['instock']} instock")
                # INFO: Loop of how many items. Breaking if valid number or 0 as input. 
                while True:
                    purchaseCounter = input("How many items do you want to request?")
                    # INFO: make sure the input is a int value. 
                    purchase = int(purchaseCounter)                                  
                    if purchase >0 and purchase <= v['instock']:
                        print("purchase handling....")
                        # INFO: handle my purchase fiunction
                        handlePurchase(k,v,purchase)
                        break

                    elif purchase < 0:
                        print("Cant be negative amounts. Try again")
                    elif purchase == 0:
                        cancel = False
                        break
                    elif purchase > v['instock']:
                            print(f"Only {v['instock']} of the product instock. Try again")

                
                inStock = True
                break
            elif v["instock"] == 0:
                inStock = False
            
        elif v["product_id"] != inputId:
            invalid = True 

    #INFO: Dump the updated instock of items in inventory file      
    try: 
        with open("data/inventory.json","w") as updatedInventory:
            json.dump(inventory, updatedInventory)          
            updatedInventory.close()
    except:
        print("Failed to update inventory")
      
    # Boolean error messages
    if cancel is False:
        print("Request is canceled")
        cancel = True
    if invalid:
        print("Could not find product. Try again with different ID")
        invalid = False
    if inStock is False:
        print("Valid id, but not in stock. Try again with different ID")
        inStock = True



def main():
    # Starting up print statement
    print("Add items to shopping cart!\n")
    
    inputId= None

    while True:
        
        inputId = input("Please enter the product ID...")
        if inputId == "q":
            break
        
        # INFO: Import and use regex to check if the input string only contains a-z A-Z and 0-9 
        # + means to match 1 or more of the preceeding token.
        pattern = re.compile("[A-Za-z0-9]+")
     
        if pattern.fullmatch(inputId) is not None:
            # INFO: Convert to uppercase BEFORE checking inventory items.
            inputId = inputId.upper()
            try:   
                # FIXED: Make persistant
                with open("data/inventory.json","r") as inputFile:
                    inventory = json.load(inputFile)  
                    inputFile.close()                   
                    handleInventory(inputId,inventory)
            except:
                return "Could not find file! "
        else:
            print("Input ID can only include A-Z, a-z and 0-9. Please try again \n")
     


if __name__ == '__main__':
    main()

