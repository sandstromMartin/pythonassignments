#!/usr/bin/env python
# coding: utf-8

# In[1]:


# custom_calc.py

hello = "hello"

def addition(number):
    return sum(number)
    
def subtraction(number):
    first = number[0]
    number.pop(0)
    
    for n in number:
        rest = sum(number)
    result = first - rest
    return result

def multiplication(number):
    result = 1
    for n in number:
        result *= n
    return result

def divison(number):
    result = number[0]
    number.pop(0)
    for n in number:
        result /= n        
    return result

def power(number):
    result = number[0]
    number.pop(0)
    for n in number:        
        result **= n
    return result
    